﻿#include <iostream>
#include <ctime>
using namespace std;

int main()
{
    int sum = 0;
    int N; //вводим размер массива
    cout << "enter size of array" << endl;
    cin >> N;

    time_t t_now = time(0); //получаем текущее число
    int day = localtime(&t_now)->tm_mday % N;

    int** matrix = new int* [N]; 

    for (int i = 0; i < N; i++) 
    {
        matrix[i] = new int[N];
        if (i == day)
        {
            for (int j = 0; j < N; j++)
            {
                matrix[i][j] = i + j; 
                sum += matrix[i][j]; 
            }
        }
        else
        {
            for (int j = 0; j < N; j++)
            {
                matrix[i][j] = i + j; 
            }
        }
    }
    cout << "Sum of elements array : " << sum; //выводим 
}